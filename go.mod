module gitlab.com/njbaron/go-session-auth

go 1.16

require (
	github.com/gomodule/redigo v1.8.5 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
)
