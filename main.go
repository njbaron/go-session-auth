package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gomodule/redigo/redis"
	uuid "github.com/nu7hatch/gouuid"
)

var cache redis.Conn

func main() {
	initCache()

	http.HandleFunc("/signin", Signin)
	http.HandleFunc("/welcome", Welcome)
	http.HandleFunc("/refresh", Refresh)

	log.Fatal(http.ListenAndServe(":8000", nil))
}

func initCache() {
	conn, err := redis.DialURL("redis://localhost")
	if err != nil {
		panic(err)
	}

	cache = conn
}

var users = map[string]string{
	"user1": "password1",
	"user2": "password2",
}

type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

func Signin(w http.ResponseWriter, r *http.Request) {
	var creds Credentials

	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println("Bad credentials json")
		return
	}

	expectedPassword, ok := users[creds.Username]
	if !ok || expectedPassword != creds.Password {
		w.WriteHeader(http.StatusUnauthorized)
		log.Printf("Invalid username/password: %s : %s\n", creds.Username, creds.Password)
	}

	sessionToken, err := uuid.NewV4()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println("Unable to generate session token")
		return
	}

	sessionTokenString := sessionToken.String()

	_, err = cache.Do("SETEX", sessionTokenString, "120", creds.Username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println("Unable to set redis cache")
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "session_token",
		Value:   sessionTokenString,
		Expires: time.Now().Add(120 * time.Second),
	})

}

func Welcome(w http.ResponseWriter, r *http.Request) {
	c, err := r.Cookie("session_token")

	if err != nil {
		if err == http.ErrNoCookie {
			w.WriteHeader(http.StatusUnauthorized)
			log.Println("Unauthorized user access")
			return
		} else {
			w.WriteHeader(http.StatusBadRequest)
			log.Println("generic unable to find cookie")
			return
		}
	}

	sessionToken := c.Value

	response, err := cache.Do("GET", sessionToken)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println("unable to get redis cache")
		return
	}
	if response == nil {
		w.WriteHeader(http.StatusUnauthorized)
		log.Println("unable to find token in redis cache")
		return
	}

	w.Write([]byte(fmt.Sprintf("Welcome %s!", response)))
}

func Refresh(w http.ResponseWriter, r *http.Request) {
	c, err := r.Cookie("session_token")

	if err != nil {
		if err == http.ErrNoCookie {
			w.WriteHeader(http.StatusUnauthorized)
			log.Println("Unauthorized user access")
			return
		} else {
			w.WriteHeader(http.StatusBadRequest)
			log.Println("generic unable to find cookie")
			return
		}
	}

	sessionToken := c.Value

	response, err := cache.Do("GET", sessionToken)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println("unable to get redis cache")
		return
	}
	if response == nil {
		w.WriteHeader(http.StatusUnauthorized)
		log.Println("unable to find token in redis cache")
		return
	}

	newSessionToken, err := uuid.NewV4()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println("unable to get new Session Token")
		return
	}
	newSessionTokenString := newSessionToken.String()

	_, err = cache.Do("SETEX", newSessionTokenString, "120", fmt.Sprintf("%s", response))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	_, err = cache.Do("DEL", sessionToken)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    "session_token",
		Value:   newSessionTokenString,
		Expires: time.Now().Add(120 * time.Second),
	})
}
